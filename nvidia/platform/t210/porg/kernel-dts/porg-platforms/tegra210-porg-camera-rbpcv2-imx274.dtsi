/*
 * Copyright (c) 2018-2019, NVIDIA CORPORATION.  All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tegra210-camera-rbpcv2-imx274.dtsi"

#define CAM1_PWDN		TEGRA_GPIO(S, 7)
#define CAM2_PWDN_GPIO		TEGRA_GPIO(T, 0)
#define CAM3_PWDN_GPIO		TEGRA_GPIO(V, 0)
#define CAM4_PWDN		TEGRA_GPIO(H, 6)
#define CAMERA_I2C_MUX_BUS(x) (0x1E + x)

/ {
    host1x {
		i2c@546c0000 {
			tca9548@70 {
				compatible = "nxp,pca9548";
				reg = <0x70>;
				#address-cells = <1>;
				#size-cells = <0>;
				vcc-supply = <&p3448_vdd_3v3_sys>;
				skip_mux_detect;
				force_bus_start = <CAMERA_I2C_MUX_BUS(0)>;
				status = "okay";
				i2c@0{
					reg = <0>;
					i2c-mux,deselect-on-exit;
					#address-cells = <1>;
					#size-cells = <0>;
					pca9570_a@24 {
						compatible = "nvidia,pca9570";
						reg = <0x24>;
						channel = "a";
						drive_ic = "DRV8838";
					};		
					imx274_a@1a {
						status = "okay";
						reset-gpios = <&gpio CAM1_PWDN GPIO_ACTIVE_HIGH>;
						/* Define any required hw resources needed by driver */
						/* ie. clocks, io pins, power sources */
						clocks = <&tegra_car TEGRA210_CLK_CLK_OUT_3>;
						clock-names = "cam_mclk1";
						clock-frequency = <24000000>;
						mclk = "cam_mclk1";
					};
				};

				i2c@1{
					reg = <1>;
					i2c-mux,deselect-on-exit;
					#address-cells = <1>;
					#size-cells = <0>;
					pca9570_c@24 {
						compatible = "nvidia,pca9570";
						reg = <0x24>;
						channel = "c";
						drive_ic = "DRV8838";
					};		
					imx274_c@1a {
						status = "okay";
						reset-gpios = <&gpio CAM2_PWDN_GPIO GPIO_ACTIVE_HIGH>;
						/* Define any required hw resources needed by driver */
						/* ie. clocks, io pins, power sources */
						clocks = <&tegra_car TEGRA210_CLK_CLK_OUT_3>;
						clock-names = "cam_mclk1";
						clock-frequency = <24000000>;
						mclk = "cam_mclk1";
					};
				};

				i2c@2{
					reg = <2>;
					i2c-mux,deselect-on-exit;
					#address-cells = <1>;
					#size-cells = <0>;
					pca9570_e@24 {
						compatible = "nvidia,pca9570";
						reg = <0x24>;
						channel = "e";
						drive_ic = "DRV8838";
					};		
					imx274_e@1a {
						status = "okay";
						reset-gpios = <&gpio CAM3_PWDN_GPIO GPIO_ACTIVE_HIGH>;
						/* Define any required hw resources needed by driver */
						/* ie. clocks, io pins, power sources */
						clocks = <&tegra_car TEGRA210_CLK_CLK_OUT_3>;
						clock-names = "cam_mclk1";
						clock-frequency = <24000000>;
						mclk = "cam_mclk1";
					};
				};

				i2c@3{
					reg = <3>;
					i2c-mux,deselect-on-exit;
					#address-cells = <1>;
					#size-cells = <0>;
					pca9570_f@24 {
						compatible = "nvidia,pca9570";
						reg = <0x24>;
						channel = "f";
						drive_ic = "DRV8838";
					};		
					imx274_f@1a {
						status = "okay";
						reset-gpios = <&gpio CAM4_PWDN GPIO_ACTIVE_HIGH>;
						/* Define any required hw resources needed by driver */
						/* ie. clocks, io pins, power sources */
						clocks = <&tegra_car TEGRA210_CLK_CLK_OUT_3>;
						clock-names = "cam_mclk1";
						clock-frequency = <24000000>;
						mclk = "cam_mclk1";
					};
				};

			};
		};
	};

    gpio@6000d000 {

		camera-control-output-low {
			gpio-hog;
			output-low;
			gpios = < CAM1_PWDN 0 >;
			label = "cam0-pwdn";
		};

		camera-control-output-low {
			gpio-hog;
			output-low;
			gpios = < CAM2_PWDN_GPIO 0 >;
			label = "cam1-pwdn";
		};


		camera-control-output-low {
			gpio-hog;
			output-low;
			gpios = < CAM3_PWDN_GPIO 0 >;
			label = "cam2-pwdn";
		};

		camera-control-output-low {
			gpio-hog;
			output-low;
			gpios = < CAM4_PWDN 0 >;
			label = "cam3-pwdn";
		};

	};
};
